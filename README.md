Västgöta Nations gamla kassasystem
==================================
Det här är Västgöta Nations i Linköpings gamla kassasystem vilket
användes i nationslokalen Ättestupan. Det är skriver av
Erik Bäcklund Ekvall.

Systemet är skrivet för Windows, men bör nu mera även fungera skapligt
på Linux.

Eftersom det här programmet är horibelt finns det nu en uppföljare på
<https://git.lysator.liu.se/hugo/Stupan>, vilken är en bra bit bättre.
